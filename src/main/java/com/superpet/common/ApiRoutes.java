package com.superpet.common;

import com.jfinal.config.Routes;
import com.superpet.weixin.api.User.UserController;
import com.superpet.weixin.api.pet.PetController;
import com.superpet.weixin.api.petCard.PetCardController;
import com.superpet.weixin.api.uploadFile.UploadFileController;
import com.superpet.weixin.api.wxrun.WxRunController;
import com.superpet.weixin.api.login.LoginController;

/**
 * 前台路由
 */
public class ApiRoutes extends Routes {

	public void config() {

		setBaseViewPath("/");
		
		add("/api/wx/login", LoginController.class);
		add("/api/wx/wxrun", WxRunController.class);
		add("/api/wx/user", UserController.class);
		add("/api/wx/uploadFile", UploadFileController.class);
		add("/api/wx/pet", PetController.class);
		add("/api/wx/petCard", PetCardController.class);
	}
}
